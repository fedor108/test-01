<?php

declare(strict_types=1);

namespace integration\Domain\Entities;

use Exception;
use Fedor108\Test01\Domain\Entities\User;
use Fedor108\Test01\Domain\Entities\UserBuilder;
use Fedor108\Test01\Domain\Exceptions\InvalidArgumentException;
use Fedor108\Test01\Domain\Services\Validation\DeletedValidator;
use Fedor108\Test01\Domain\Services\Validation\EmailValidator;
use Fedor108\Test01\Domain\Services\Validation\NameValidator;
use Fedor108\Test01\Infrastructure\Repositories\InvalidDomainRepository;
use Fedor108\Test01\Infrastructure\Repositories\InvalidNameRepository;
use PHPUnit\Framework\TestCase;

class UserBuilderTest extends TestCase
{
    public const VALID_USER_NAME = 'validusername';
    public const INVALID_USER_NAME = 'invalidusername';
    public const VALID_USER_EMAIL = 'valid@valid.com';
    public const INVALID_DOMAIN = 'invalid.com';

    private UserBuilder $userBuilder;

    public function setUp(): void
    {
        parent::setUp();

        $invalidNameRepository = new InvalidNameRepository();
        $invalidNameRepository->add(self::INVALID_USER_NAME);

        $invalidDomainRepository = new InvalidDomainRepository();
        $invalidDomainRepository->add(self::INVALID_DOMAIN);

        $this->userBuilder = new UserBuilder(
            new NameValidator($invalidNameRepository),
            new EmailValidator($invalidDomainRepository),
        );
    }

    /**
     * @test
     * @throws Exception
     */
    public function throwIfNameInInvalidNameRepository(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage(NameValidator::NAME_IN_STOP_LIST_MESSAGE . self::INVALID_USER_NAME);

        $this->userBuilder->build([
            User::KEY_NAME => self::INVALID_USER_NAME,
            User::KEY_EMAIL => self::VALID_USER_EMAIL,
        ]);
    }

    /**
     * @test
     * @throws Exception
     */
    public function throwIfEmailInInvalidDomainRepository(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage(EmailValidator::DOMAIN_IN_STOP_LIST_MESSAGE . self::INVALID_DOMAIN);

        $this->userBuilder->build([
            User::KEY_NAME => self::VALID_USER_NAME,
            User::KEY_EMAIL => self::VALID_USER_NAME . '@' . self::INVALID_DOMAIN,
        ]);
    }
}
