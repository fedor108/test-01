<?php

declare(strict_types=1);

namespace unit\Domain\Entities;

use Exception;
use Fedor108\Test01\Domain\Entities\User;
use Fedor108\Test01\Domain\Entities\UserBuilder;
use Fedor108\Test01\Domain\Exceptions\InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public const VALID_USER_NAME = 'validusername';
    public const VALID_USER_EMAIL = 'email@email.com';
    public const INVALID_TOO_SHORT_USER_NAME = 'short';
    public const INVALID_SYMBOL_USER_NAME = '!user_name';

    private UserBuilder $userBuilder;

    public function setUp(): void
    {
        parent::setUp();

        $this->userBuilder = new UserBuilder();
    }

    /**
     * @test
     * @throws Exception
     */
    public function canInstantiateUser(): void
    {
        $email = self::VALID_USER_EMAIL;
        $name = self::VALID_USER_NAME;

        $user = $this->userBuilder->build([
            User::KEY_NAME => $name,
            User::KEY_EMAIL => $email,
        ]);

        $this->assertEquals($name,  $user->getName());
        $this->assertEquals($email,  $user->getEmail());
    }

    /**
     * @test
     * @dataProvider canCreateFromArrayDataProvider
     * @throws Exception
     */
    public function canCreateFromArray($arrayToCreateUser): void
    {
        $user = $this->userBuilder->build($arrayToCreateUser);

        $arrayOfCreatedUser = $user->jsonSerialize();

        foreach ($arrayToCreateUser as $key => $value) {
            $this->assertEquals($value, $arrayOfCreatedUser[$key]);
        }
    }

    /**
     * @test
     * @dataProvider trowIfInvalidNameDataProvider
     * @throws Exception
     */
    public function throwIfInvalidName($name): void
    {
        $this->expectException(InvalidArgumentException::class);

        $this->userBuilder->build([
            User::KEY_NAME => $name,
            User::KEY_EMAIL => self::VALID_USER_EMAIL,
        ]);
    }

    public function canCreateFromArrayDataProvider(): array
    {
        $created = date(User::ARRAY_DATE_FORMAT);
        $deleted = date(User::ARRAY_DATE_FORMAT, strtotime($created) + 1);
        $name = self::VALID_USER_NAME;
        $email = self::VALID_USER_EMAIL;

        return [
            [[
                User::KEY_NAME => $name,
                User::KEY_EMAIL => $email,
            ]],
            [[
                User::KEY_ID => time(),
                User::KEY_NAME => $name,
                User::KEY_EMAIL => $email,
                User::KEY_NOTES => 'some notes',
                User::KEY_CREATED => $created,
                User::KEY_DELETED => $deleted,
            ]],
        ];
    }

    public function trowIfInvalidNameDataProvider(): array
    {
        return [
            [self::INVALID_SYMBOL_USER_NAME, ''],
            [self::INVALID_TOO_SHORT_USER_NAME, ''],
        ];
    }
}
