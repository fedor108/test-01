<?php

declare(strict_types=1);

namespace Fedor108\Test01\Domain\Exceptions;

use Exception;

class InvalidArgumentException extends Exception
{
}
