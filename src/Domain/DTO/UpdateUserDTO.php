<?php

declare(strict_types=1);

namespace Fedor108\Test01\Domain\DTO;

use JsonSerializable;

class UpdateUserDTO implements JsonSerializable
{
    public function __construct(
        public int $id,
        public ?string $email,
        public ?string $name,
        public ?string $notes,
    ) {
    }

    public function jsonSerialize()
    {
        return array_filter([
            'id' => $this->id,
            'email' => $this->email,
            'name' => $this->name,
            'notes' => $this->notes,
        ]);
    }
}
