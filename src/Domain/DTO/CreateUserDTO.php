<?php

declare(strict_types=1);

namespace Fedor108\Test01\Domain\DTO;

use JsonSerializable;

class CreateUserDTO implements JsonSerializable
{
    public function __construct(
        public string $email,
        public string $name,
        public ?string $notes,
    ) {
    }

    public function jsonSerialize()
    {
        return array_filter([
            'email' => $this->email,
            'name' => $this->name,
            'notes' => $this->notes,
        ]);
    }
}
