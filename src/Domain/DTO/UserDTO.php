<?php

declare(strict_types=1);

namespace Fedor108\Test01\Domain\DTO;

use DateTime;
use Fedor108\Test01\Domain\Entities\User;
use JsonSerializable;

class UserDTO implements JsonSerializable
{
    public const DATE_TIME_FORMAT = 'Y-m-d H:i:s';

    public int $id;
    public string $email;
    public string $name;
    public ?string $notes;
    public DateTime $created;
    public ?DateTime $deleted;

    public function __construct(User $user)
    {
        $this->id = $user->getId();
        $this->email = $user->getEmail();
        $this->name = $user->getName();
        $this->notes = $user->getNotes();
        $this->created = $user->getCreated();
        $this->deleted = $user->getDeleted();
    }

    public function jsonSerialize()
    {
        return array_filter([
            'id' => $this->id,
            'email' => $this->email,
            'name' => $this->name,
            'notes' => $this->notes,
            'created' => $this->created->format(self::DATE_TIME_FORMAT),
            'deleted' => $this->deleted?->format(self::DATE_TIME_FORMAT),
        ]);
    }
}
