<?php

declare(strict_types=1);

namespace Fedor108\Test01\Domain;

interface InvalidNameRepositoryInterface
{
    public function has(string $name): bool;

    public function add(string $name): void;
}
