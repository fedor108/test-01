<?php

declare(strict_types=1);

namespace Fedor108\Test01\Domain\Entities;

use DateTime;
use Exception;
use Fedor108\Test01\Domain\Services\Validation\DeletedValidator;
use Fedor108\Test01\Domain\Services\Validation\EmailValidator;
use Fedor108\Test01\Domain\Services\Validation\NameValidator;
use Fedor108\Test01\Domain\Services\Validation\ValidatorInterface;
use Fedor108\Test01\Infrastructure\Repositories\InvalidDomainRepository;
use Fedor108\Test01\Infrastructure\Repositories\InvalidNameRepository;

class UserBuilder
{
    public function __construct(
        private ?ValidatorInterface $nameValidator = null,
        private ?ValidatorInterface $emailValidator = null,
        private ?ValidatorInterface $deletedValidator = null,
    ) {
        if (!$this->nameValidator) {
            $this->nameValidator = new NameValidator(new InvalidNameRepository());
        }

        if (!$this->emailValidator) {
            $this->emailValidator = new EmailValidator(new InvalidDomainRepository());
        }

        if (!$this->deletedValidator) {
            $this->deletedValidator = new DeletedValidator();
        }
    }

    /**
     * @throws Exception
     */
    public function build(array $data): User
    {
        $user = new User(
            $this->nameValidator,
            $this->emailValidator,
            $this->deletedValidator,
        );

        $user->setName($data[User::KEY_NAME]);
        $user->setEmail($data[User::KEY_EMAIL]);

        if (isset($data[User::KEY_ID])) {
            $user->setId($data[User::KEY_ID]);
        }

        if (isset($data[User::KEY_NOTES])) {
            $user->setNotes($data[User::KEY_NOTES]);
        }

        if (isset($data[User::KEY_CREATED])) {
            $user->setCreated(new DateTime($data[User::KEY_CREATED]));
        }

        if (isset($data[User::KEY_DELETED])) {
            $user->setDeleted(new DateTime($data[User::KEY_DELETED]));
        }

        return $user;
    }
}
