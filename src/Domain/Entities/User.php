<?php

declare(strict_types=1);

namespace Fedor108\Test01\Domain\Entities;

use DateTime;
use Fedor108\Test01\Domain\Services\Validation\ValidatorInterface;
use JsonSerializable;

class User implements JsonSerializable
{
    public const ARRAY_DATE_FORMAT = 'Y-m-d H:i:s';

    public const KEY_ID = 'id';
    public const KEY_NAME = 'name';
    public const KEY_EMAIL = 'email';
    public const KEY_NOTES = 'notes';
    public const KEY_CREATED = 'created';
    public const KEY_DELETED = 'deleted';

    private string $name;
    private string $email;
    private ?string $notes = null;
    private ?int $id = null;
    private ?DateTime $created = null;
    private ?DateTime $deleted = null;

    public function __construct(
        private ValidatorInterface $nameValidator,
        private ValidatorInterface $emailValidator,
        private ValidatorInterface $deletedValidator,
    ) {
    }

    public function getCreated(): ?DateTime
    {
        return $this->created;
    }

    public function getDeleted(): ?DateTime
    {
        return $this->deleted;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setCreated(DateTime $created): void
    {
        $this->created = $created;
    }

    public function setDeleted(DateTime $deleted): void
    {
        $this->deletedValidator->validate($this->created, $deleted);
        $this->deleted = $deleted;
    }

    public function setEmail(string $email): void
    {
        $this->emailValidator->validate($email);
        $this->email = $email;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function setName(string $name): void
    {
        $this->nameValidator->validate($name);
        $this->name = $name;
    }

    public function setNotes(?string $notes): void
    {
        $this->notes = $notes;
    }

    public function setNameValidator(ValidatorInterface $nameValidator): void
    {
        $this->nameValidator = $nameValidator;
    }

    public function setEmailValidator(ValidatorInterface $emailValidator): void
    {
        $this->emailValidator = $emailValidator;
    }

    public function jsonSerialize()
    {
        return array_filter([
            self::KEY_ID => $this->getId(),
            self::KEY_NAME => $this->getName(),
            self::KEY_EMAIL => $this->getEmail(),
            self::KEY_NOTES => $this->getNotes(),
            self::KEY_DELETED => $this->getDeleted()?->format(self::ARRAY_DATE_FORMAT),
            self::KEY_CREATED => $this->getCreated()?->format(self::ARRAY_DATE_FORMAT),
        ]);
    }
}
