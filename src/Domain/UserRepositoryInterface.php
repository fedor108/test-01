<?php

declare(strict_types=1);

namespace Fedor108\Test01\Domain;

use Fedor108\Test01\Domain\Entities\User;

interface UserRepositoryInterface
{
    public function get(int $userId): User;

    public function create(User $user): User;

    public function update(User $user): User;

    public function delete(int $userId): void;
}
