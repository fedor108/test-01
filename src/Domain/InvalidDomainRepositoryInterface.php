<?php

declare(strict_types=1);

namespace Fedor108\Test01\Domain;

interface InvalidDomainRepositoryInterface
{
    public function has(string $domain): bool;

    public function add(string $domain): void;
}
