<?php

declare(strict_types=1);

namespace Fedor108\Test01\Domain\Services\Validation;

use Fedor108\Test01\Domain\Exceptions\InvalidArgumentException;
use Fedor108\Test01\Domain\InvalidNameRepositoryInterface;

class NameValidator implements ValidatorInterface
{
    public const NAME_IN_STOP_LIST_MESSAGE = 'User name is in a stop list: ';

    private const MIN_LENGTH = 8;

    public function __construct(
        private InvalidNameRepositoryInterface $invalidNameRepository
    ) {
    }

    /**
     * @throws InvalidArgumentException
     */
    public function validate(...$args): void
    {
        $name = $args[0] ?? null;

        if (!$name) {
            throw new InvalidArgumentException('User name can not be empty');
        }

        if (strlen($name) < self::MIN_LENGTH) {
            throw new InvalidArgumentException(sprintf(
                'The user name "%s" length can not be shorter than %d',
                $name,
                self::MIN_LENGTH,
            ));
        }

        if (preg_match('/[^a-z\-0-9]/i', $name)) {
            throw new InvalidArgumentException(sprintf(
                'The user name "%s" must be alphanumeric',
                $name,
            ));
        }

        if ($this->invalidNameRepository->has($name)) {
            throw new InvalidArgumentException(self::NAME_IN_STOP_LIST_MESSAGE . $name);
        }
    }
}
