<?php

declare(strict_types=1);

namespace Fedor108\Test01\Domain\Services\Validation;

use Fedor108\Test01\Domain\Entities\User;
use Fedor108\Test01\Domain\Exceptions\InvalidArgumentException;

class DeletedValidator implements ValidatorInterface
{
    /**
     * @throws InvalidArgumentException
     */
    public function validate(...$args): void
    {
        $created = $args[0] ?? null;
        $deleted = $args[1] ?? null;

        if (!$deleted) {
            return;
        }

        if (!$created) {
            throw new InvalidArgumentException('Can not validate deleted if created is null');
        }

        if ($deleted < $created) {
            throw new InvalidArgumentException(sprintf(
                'Deleted %s must be greater or equal than created %s',
                $deleted->format(User::ARRAY_DATE_FORMAT),
                $created->format(User::ARRAY_DATE_FORMAT),
            ));
        }
    }
}
