<?php

declare(strict_types=1);

namespace Fedor108\Test01\Domain\Services\Validation;

use Fedor108\Test01\Domain\Exceptions\InvalidArgumentException;
use Fedor108\Test01\Domain\InvalidDomainRepositoryInterface;

class EmailValidator implements ValidatorInterface
{
    public const DOMAIN_IN_STOP_LIST_MESSAGE = 'The email domain is in a stop list: ';

    public function __construct(
        private InvalidDomainRepositoryInterface $invalidUserEmailDomainRepository,
    ) {
    }

    /**
     * @throws InvalidArgumentException
     */
    public function validate(...$args): void
    {
        $email = $args[0] ?? null;

        if (!$email) {
            throw new InvalidArgumentException('Email can not be empty: ' . $email);
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new InvalidArgumentException('Invalid email format: ' . $email);
        }

        $domain = $this->getDomain($email);
        if ($this->invalidUserEmailDomainRepository->has($domain)) {
            throw new InvalidArgumentException(self::DOMAIN_IN_STOP_LIST_MESSAGE . $domain);
        }
    }

    private function getDomain(string $email): string
    {
        $array = explode('@', $email);
        return array_pop($array);
    }
}
