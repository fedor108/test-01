<?php

declare(strict_types=1);

namespace Fedor108\Test01\Domain\Services\Validation;

interface ValidatorInterface
{
    public function validate(...$args): void;
}
