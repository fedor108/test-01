<?php

declare(strict_types=1);

namespace Fedor108\Test01\Domain\Services;

use SplObserver;
use SplSubject;

class UserEventLogger implements SplObserver
{
    public function update(SplSubject $subject, ?string $event = null, ?array $data = null)
    {
        // TODO: Implement update() method.
    }
}
