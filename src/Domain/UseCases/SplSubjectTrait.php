<?php

declare(strict_types=1);

namespace Fedor108\Test01\Domain\UseCases;

use SplObserver;

trait SplSubjectTrait
{
    /** @var array<SplObserver> */
    private array $observers = [];

    public function attach(SplObserver $observer): void
    {
        $this->observers[] = $observer;
    }

    public function detach(SplObserver $observer): void
    {
        foreach ($this->observers as $key => $item) {
            if ($observer === $item) {
                unset($this->observers[$key]);
            }
        }
    }

    public function notify(?string $event = null, ?array $data = null): void
    {
        foreach ($this->observers as $observer) {
            $observer->update($this, $event, $data);
        }
    }
}