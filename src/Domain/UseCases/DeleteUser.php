<?php

declare(strict_types=1);

namespace Fedor108\Test01\Domain\UseCases;

use Fedor108\Test01\Domain\UserRepositoryInterface;
use SplSubject;

class DeleteUser implements SplSubject
{
    use SplSubjectTrait;

    public function __construct(
        private UserRepositoryInterface $userRepository
    ) {
    }

    public function execute(int $userId): void
    {
        $this->userRepository->delete($userId);

        $this->notify('user deleted', ['id' => $userId]);
    }
}