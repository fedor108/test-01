<?php

declare(strict_types=1);

namespace Fedor108\Test01\Domain\UseCases;

use Fedor108\Test01\Domain\DTO\UpdateUserDTO;
use Fedor108\Test01\Domain\DTO\UserDTO;
use Fedor108\Test01\Domain\Exceptions\InvalidArgumentException;
use Fedor108\Test01\Domain\UserRepositoryInterface;
use SplSubject;

class UpdateUser implements SplSubject
{
    use SplSubjectTrait;

    public function __construct(
        private UserRepositoryInterface $userRepository,
    ) {
    }

    /**
     * @throws InvalidArgumentException
     */
    public function execute(UpdateUserDTO $dto): UserDTO
    {
        $user = $this->userRepository->get($dto->id);

        if ($dto->name) {
            $user->setName($dto->name);
        }

        if ($dto->email) {
            $user->setEmail($dto->email);
        }

        if ($dto->notes) {
            $user->setNotes($dto->notes);
        }

        $updatedUser = $this->userRepository->update($user);

        $this->notify('user updated', $dto->jsonSerialize());

        return new UserDTO($updatedUser);
    }

}
