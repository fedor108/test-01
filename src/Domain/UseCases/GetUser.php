<?php

declare(strict_types=1);

namespace Fedor108\Test01\Domain\UseCases;

use Fedor108\Test01\Domain\DTO\UserDTO;
use Fedor108\Test01\Domain\UserRepositoryInterface;

class GetUser
{
    public function __construct(
        private UserRepositoryInterface $userRepository
    ) {
    }

    public function execute(int $userId): UserDTO
    {
        $user = $this->userRepository->get($userId);

        return new UserDTO($user);
    }
}