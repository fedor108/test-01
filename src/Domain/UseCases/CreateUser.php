<?php

declare(strict_types=1);

namespace Fedor108\Test01\Domain\UseCases;

use Exception;
use Fedor108\Test01\Domain\DTO\CreateUserDTO;
use Fedor108\Test01\Domain\DTO\UserDTO;
use Fedor108\Test01\Domain\Entities\UserBuilder;
use Fedor108\Test01\Domain\UserRepositoryInterface;

class CreateUser
{
    public function __construct(
        private UserRepositoryInterface $userRepository,
        private UserBuilder $userBuilder,
    ) {
    }

    /**
     * @throws Exception
     */
    public function execute(CreateUserDTO $dto): UserDTO
    {
        $user = $this->userRepository->create($this->userBuilder->build($dto->jsonSerialize()));

        return new UserDTO($user);
    }
}
