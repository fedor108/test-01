<?php

declare(strict_types=1);

namespace Fedor108\Test01\Infrastructure\Repositories\MySqlUserRepository\Exceptions;

use Exception;

class NotFoundInRepositoryException extends Exception
{
}
