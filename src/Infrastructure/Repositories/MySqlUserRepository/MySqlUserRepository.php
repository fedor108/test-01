<?php

declare(strict_types=1);

namespace Fedor108\Test01\Infrastructure\Repositories\MySqlUserRepository;

use DateTime;
use Exception;
use Fedor108\Test01\Domain\Entities\User;
use Fedor108\Test01\Domain\Entities\UserBuilder;
use Fedor108\Test01\Domain\Exceptions\InvalidArgumentException;
use Fedor108\Test01\Domain\UserRepositoryInterface;
use Fedor108\Test01\Infrastructure\Persistence\MySqlUserPersistence;
use Fedor108\Test01\Infrastructure\Repositories\MySqlUserRepository\Exceptions\CanNotCreateInRepositoryException;
use Fedor108\Test01\Infrastructure\Repositories\MySqlUserRepository\Exceptions\CanNotUpdateInRepositoryException;
use Fedor108\Test01\Infrastructure\Repositories\MySqlUserRepository\Exceptions\NotFoundInRepositoryException;
use Throwable;

class MySqlUserRepository implements UserRepositoryInterface
{
    private array $users;

    public function __construct(
        private MySqlUserPersistence $userPersistence,
        private UserBuilder $userBuilder,
    ) {
    }

    /**
     * @throws NotFoundInRepositoryException
     */
    public function get(int $userId): User
    {
        if (!$this->users[$userId]) {
            $this->users[$userId] = $this->userPersistence->retrieve($userId);
        }

        if (!$this->users[$userId]) {
            throw new NotFoundInRepositoryException('Not found user with ID: ' . $userId);
        }

        return $this->users[$userId];
    }

    /**
     * @throws CanNotCreateInRepositoryException
     * @throws Exception
     */
    public function create(User $user): User
    {
        try {
            $row = $this->userPersistence->insert($user);
        } catch (Throwable $e) {
            throw new CanNotCreateInRepositoryException($e->getMessage());
        }

        $newUser = $this->userBuilder->build($row);

        $this->users[$newUser->getId()] = $newUser;

        return $newUser;
    }

    /**
     * @throws CanNotUpdateInRepositoryException
     * @throws Exception
     */
    public function update(User $user): User
    {
        try {
            $row = $this->userPersistence->update($user);
        } catch (Throwable $e) {
            throw new CanNotUpdateInRepositoryException($e->getMessage());
        }

        $updatedUser = $this->userBuilder->build($row);

        $this->users[$updatedUser->getId()] = $updatedUser;

        return $updatedUser;
    }

    /**
     * @throws NotFoundInRepositoryException
     * @throws CanNotUpdateInRepositoryException
     * @throws InvalidArgumentException
     */
    public function delete(int $userId): void
    {
        $user = $this->get($userId);

        $user->setDeleted(new DateTime());

        $this->update($user);

        unset($this->users[$user->getId()]);
    }
}
