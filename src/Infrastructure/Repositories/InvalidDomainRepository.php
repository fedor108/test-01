<?php

declare(strict_types=1);

namespace Fedor108\Test01\Infrastructure\Repositories;

use Fedor108\Test01\Domain\InvalidDomainRepositoryInterface;

class InvalidDomainRepository implements InvalidDomainRepositoryInterface
{
    private array $domains = [];

    public function has(string $domain): bool
    {
        return in_array($domain, $this->domains);
    }

    public function add(string $domain): void
    {
        if (!in_array($domain, $this->domains)) {
            $this->domains[] = $domain;
        }
    }
}
