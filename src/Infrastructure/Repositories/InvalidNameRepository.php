<?php

declare(strict_types=1);

namespace Fedor108\Test01\Infrastructure\Repositories;

use Fedor108\Test01\Domain\InvalidNameRepositoryInterface;

class InvalidNameRepository implements InvalidNameRepositoryInterface
{
    /**
     * @var array<string>
     */
    private array $names = [];

    public function has(string $name): bool
    {
        return in_array($name, $this->names);
    }

    public function add(string $name): void
    {
        if (!in_array($name, $this->names)) {
            $this->names[] = $name;
        }
    }
}
