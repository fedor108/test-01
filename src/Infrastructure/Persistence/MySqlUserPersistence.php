<?php

declare(strict_types=1);

namespace Fedor108\Test01\Infrastructure\Persistence;

use Fedor108\Test01\Domain\Entities\User;

class MySqlUserPersistence
{
    public function retrieve(int $userId): ?array
    {
        // TODO: write code to retrieve row
        return [];
    }

    public function insert(User $user): array
    {
        // TODO: insert to user table and return new row
        return [];
    }

    public function update(User $user): array
    {
        // TODO: update users where id = :id and return updated row
        return [];
    }
}
